@echo off

setlocal EnableDelayedExpansion

set "ScriptPath=%~dp0"

set "BinFile=%~f1"

if not defined BinFile (
	echo Usage: %~nx0 ^<binary path^> ^[^<api key path^>^]
	goto End
) else (
	if "%~x1" == "" set "BinFile=%BinFile%.exe"
)

if exist "%BinFile%" (
	set "BinFileName=%~dpn1"
) else (
	echo Invalid binary path entered: "%BinFile%"
	goto End
)

set "KeyFile=%~f2"
if not defined KeyFile set "KeyFile=%ScriptPath%\.api_key"

if exist "%KeyFile%" (
	set /p API_Key=<"%KeyFile%"
) else (
	echo Missing API Key file, you can't use the symstore without a valid key!
	goto End
)

if defined ProgramFiles(x86) (
	set "BinArch=x64"
) else (
	set "BinArch=x86"
)

pushd "%ScriptPath%\windows\"

	bin\%BinArch%\symstore.exe add /compress -:NOREFS /f "%BinFile%" /s "tmp" /t "symstore"
	bin\%BinArch%\symstore.exe add /compress -:NOREFS /f "%BinFileName%.pdb" /s "tmp" /t "symstore"

	rmdir /s /q "tmp\000Admin"
	del /f /q "tmp\pingme.txt"

	for /r %%a in (*.*_) do (
		set "File=%%a"
		set "File=!File:%CD%\=!"
		set "File=!File:\=\\!"
		set "FilePath=%%~dpa"
		set "FilePath=!FilePath:%CD%\tmp\=!"
		set "FilePath=!FilePath:\=/!"
		<nul set /p=Uploading "!File!"... 
		curl --silent --location --write-out "%%{http_code}\n" --header "Accept: application/json" --header "X-API-Key: %API_Key%" --user-agent "symstore/1.0 (windows)" --upload-file "!File!" "https://symstore.yas-online.net/windows/!FilePath!"
	)

	rmdir /s /q "tmp"
popd

:End
endlocal
