#!/bin/bash

ScriptPath=$( cd "$(dirname "$0")"; pwd -P )

if [[ ! -e /usr/bin/realpath ]]; then
	echo "realpath is missing, it's part of 'coreutils', please install it!" >&2
	exit 69 # EX_UNAVAILABLE
fi

if [[ ! -e /usr/bin/eu-unstrip ]]; then
	echo "eu-unstrip is missing, it's part of 'elfutils', please install it!" >&2
	exit 69 # EX_UNAVAILABLE
fi

if [[ ! -e /usr/bin/curl ]]; then
	echo "curl is missing, please install it!" >&2
	exit 69 # EX_UNAVAILABLE
fi

if [[ $# < 1 ]]; then
	echo "Usage: `basename $0` <binary path> [<api key path>]" >&2
	exit 64 # EX_USAGE
fi

BinFile=$( realpath --quiet "$1" )
if [[ ! -e "$BinFile" ]]; then
	if [[ -e "$BinFile.so" ]]; then
		BinFile=${BinFile}.so
	fi
fi

if [[ ! -e "$BinFile" ]]; then
	echo "Invalid binary path entered: $BinFile" >&2
	exit 69 # EX_UNAVAILABLE
fi

IFS=' ' read -a DebugInfo <<< "$( eu-unstrip --list-only --executable $BinFile )"
BuildID=${DebugInfo[1]%@*}
BinFile=${DebugInfo[2]}
DebugFile=${DebugInfo[3]}

KeyFile=${2:-$ScriptPath/.api_key}
KeyFile=$( realpath --quiet "$KeyFile" )

if [[ ! -e "$KeyFile" ]]; then
	echo "Missing API key file, you can't use symstore without a valid key!" >&2
	exit 69 # EX_UNAVAILABLE
fi

read -r API_Key < $KeyFile

echo -n "Uploading $BinFile & $DebugFile... "
curl --silent --location --write-out "%{http_code}\n" --header "Accept: application/json" --header "X-API-Key: $API_Key" --user-agent "symstore/1.0 (linux)" --upload-file "{$BinFile,$DebugFile}" "https://symstore.yas-online.net/linux/.build-id/${BuildID:0:2}/${BuildID:2}/"
